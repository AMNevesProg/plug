var express = require('express');
var app = express();
var router = express.Router();

var path = __dirname + '/views/';

app.use('/', router);
app.use(express.static('public'))

router.get('/', function (req, res) {
    res.sendFile(path + 'index.html');
});


app.listen(8080, function () {
    console.log('Server running at Port 3000');
});